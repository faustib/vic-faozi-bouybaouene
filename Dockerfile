FROM tomcat:9-jdk11-corretto
ADD vic-2.0.war /usr/local/tomcat/webapps/
EXPOSE 8080
CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]