DROP TABLE IF EXISTS tipo_carne;

CREATE TABLE tipo_carne(
	id INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(250) NOT NULL,
	COMENTARIO VARCHAR(250) NOT NULL
);

INSERT INTO tipo_carne(nombre, comentario) VALUES
	('Ternera','De nuestras granjas');
INSERT INTO tipo_carne(nombre, comentario) VALUES
	('Pollo','Criados en comunidad');
INSERT INTO tipo_carne(nombre, comentario) VALUES
	('Cerdo','Hasta los andares se come');