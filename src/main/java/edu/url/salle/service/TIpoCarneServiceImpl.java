package edu.url.salle.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.url.salle.model.TipoCarne;
import edu.url.salle.repository.TipoCarneRepository;

@Service
public class TIpoCarneServiceImpl implements TipoCarneService {

	@Autowired
	TipoCarneRepository tipoCarneRepository;
	
	public List<TipoCarne> listarTiposCarne() {

		List<TipoCarne> listaTipoCarne = tipoCarneRepository.findAll();
		
		return (listaTipoCarne);
	}

}
